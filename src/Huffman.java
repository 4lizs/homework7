package src;

import java.util.*;


/**
 * Prefix codes and Huffman tree. Tree depends on source data.
 */
public class Huffman {
	int length; //length of encoded data
	HashMap<Byte, String> encoding; // hashMap of bytes and their associated code
	HashMap<Byte, Integer> freqs; // hashMap of the bytes and their associated frequencies
	/**
	 * Constructor to build the Huffman code for a given bytearray.
	 * 
	 * @param original
	 *            source data
	 */
	public Huffman(byte[] original) {
		if (original == null) {
			throw new RuntimeException("Input is Null!");
		}
	}

	/**
	 * Length of encoded data in bits.
	 * 
	 * @return number of bits
	 */
	public int bitLength() {
		return this.length; // TODO!!!
	}

	/**
	 * Encoding the byte array using this prefixcode.
	 * 
	 * @param origData
	 *            original data
	 * @return encoded data
	 */
	public byte[] encode(byte[] origData) {
		this.freqs = getLetterFrequencies( origData );
        Node root = buildHuffmanTree( freqs );
        this.encoding = getEncoding(root);
        StringBuffer tmp = new StringBuffer();
        for (byte b : origData) {
			tmp.append(encoding.get(b));
		}
        length = tmp.length();
        List<Byte> bytes = new ArrayList<Byte>();
		while (tmp.length() > 0) {
			while (tmp.length() < 8)
				tmp.append('0');
			String str = tmp.substring(0, 8);
			bytes.add((byte) Integer.parseInt(str, 2));
			tmp.delete(0, 8);
		}
		byte[] ret = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++){
			
			ret[i] = bytes.get(i);
		}
		return ret;
	}

	/**
	 * Decoding the byte array using this prefixcode.
	 * 
	 * @param encodedData
	 *            encoded data
	 * @return decoded data (hopefully identical to original)
	 */
	public byte[] decode(byte[] encodedData) {
		StringBuffer tmp = new StringBuffer();
		for (int i = 0; i < encodedData.length; i++){
			
			tmp.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
		}
		String str = tmp.substring(0, length);
		List<Byte> bytes = new ArrayList<Byte>();
		String code = "";
		while (str.length() > 0) {
			code += str.substring(0, 1);
			str = str.substring(1);
			Iterator<Byte> list = encoding.keySet().iterator();
			while (list.hasNext()) {
				Byte leaf = list.next();
				if (encoding.get(leaf).equals(code)) {
					bytes.add(leaf);
					code = "";
					break;
				}
			}
		}
		byte[] ret = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++){
			ret[i] = bytes.get(i);
		}
		return ret; // TODO!!!
	}

	/** Main method. */
	public static void main(String[] params) {
		String tekst = "AAA";
		byte[] orig = tekst.getBytes();
		Huffman huf = new Huffman(orig);
		byte[] kood = huf.encode(orig);
		byte[] orig2 = huf.decode(kood);
		// must be equal: orig, orig2
		System.out.println(Arrays.equals(orig, orig2));
		int lngth = huf.bitLength();
		System.out.println("Length of encoded data in bits: " + lngth);
		// TODO!!! Your tests here!
	}

    
    /**
     * computes the frequency associated with each letter found in the byte[] a.
     * @param a the byte[] containing the bytes to encode.
     * @return a hashMap of the bytes and their associated frequencies (as ints)
     */
    private static HashMap<Byte, Integer> getLetterFrequencies( byte[] a ) {
    		
            HashMap<Byte, Integer> freq = new HashMap<Byte, Integer>();
            for ( int i=0; i< a.length; i++ ) {
                    byte c = a[i];
                    if ( freq.containsKey( c ) )
                            freq.put( c, freq.get( c )+1 );
                    else
                            freq.put( c,  1 );
            }
            return freq;
    }
    
    /**
     * buildHuffmanTree: builds a Huffman tree 
     * @param freqs the hashmap of frequencies associated with each byte.
     * @return the root of the Huffman tree.
     */
    private static Node buildHuffmanTree( HashMap<Byte, Integer> freqs ) {
            PriorityQueue<Node> prioQ = new PriorityQueue<Node>();
            for ( byte c: freqs.keySet() ) 
                    prioQ.add( new Node( c, freqs.get(c), null, null ) );
            
            while ( prioQ.size() > 1 ) {
                    Node left = prioQ.poll();
                    Node right = prioQ.poll();
                    prioQ.add( new Node( (byte) '#', left.freq + right.freq, left, right ) );
            }
            
            return prioQ.poll();
    }

    /**
     * gets the string of 0s and 1s associated with each byte. 
     * @param root
     * @return a hashMap of characters and their associated code.
     */
    private static HashMap<Byte, String> getEncoding(Node root) {
            HashMap<Byte, String> encoding = new HashMap<Byte, String>();
            DFS( root, "", encoding );
            return encoding;
    }

    /**
     * Recursively performs a DFS to visit the Huffman tree and assign code to each
     * leaf. 
     * @param node
     * @param code
     * @param encoding
     */
    private static void DFS(Node node, String code, HashMap<Byte, String> encoding) {
            if ( node.isLeaf() ) {
            	code = (code.length() > 0) ? code.toString() : "0";
            	encoding.put( node.value, code );
            	
            }
            		
            else {
                    if ( node.left != null ) 
                            DFS( node.left, code+"0", encoding );
                    if ( node.right != null )
                            DFS( node.right, code+"1", encoding );
            }
    }
    /**
     * the node used to create the Huffman tree
     */
    static class Node implements Comparable {
            public byte value;                             // the value from the string, or '#' if inner node
            public int freq;                                // counts number of occurrences
            public Node left;                               // pointer to left child, if any
            public Node right;                              // pointer to right child, if nay
            
            Node( byte l, int f, Node lft, Node rt ) {
                    value = l; freq = f; left = lft; right = rt; 
            }
            /**
             * returns whether node is a leaf.
             * @return true if leaf, false otherwise.
             */
            public boolean isLeaf() { return left==null && right==null; }
            @Override
            /**
             * compareTo: needed because nodes will be kept in priority queue that
             * keeps node with smallest freq value at the root. 
             */
            public int compareTo(Object o) {
                    return freq - ((Node) o).freq;
            }
    }

}